<?php include("doctype.php"); ?>
<?php include ("layouts/header.php"); ?>

<!--                НАЧАЛО ГЛАВНОГО МЕНЮ                -->
    <div  class="header_bg">
                    <div class="container-fluid">
                        <div style="background-color: #F0F7E8" class="header row">
                            <nav class="navbar" role="navigation">
                                <div class="container-fluid" >
                                    <!-- переключатели объединяются для лучшего отображения на мобильных устройствах-->
                                    <div class="navbar-header">
                                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                                            <span class="sr-only">Переключити навігацію</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                        </button>
                                        <a class="navbar-brand" href="index.php"> </a>
                                    </div>
                                    <!-- Нафигационные ссылки и другой контент для переключения -->
                                    <div  class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                        <ul class="menu nav navbar-nav ">
                                            <li class="active"><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                                            <li><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                                            <li><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                                            <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                                            <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
                                        </ul>
                                        <form class="navbar-form navbar-right" action="search.php" method="post" onsubmit="return false;">
                                                <input type="text" class="form-control" placeholder='<?php echo  $row['search_button'] ?>' name='search' value='' id="search"><button type="submit" class="search_button btn btn-default" onclick="search();"><i class="fa fa-search" aria-hidden="true"></i>
                                        </form>
                                        <script src="js/search.js"></script>
                                    </div><!-- /.navbar-collapse -->
                                </div><!-- /.container-fluid -->
                            </nav>
                        </div>
                    </div>
                </div>
                 <!--finish header -->
                <!-- start main -->
                <?php
                    if(auth() == "user" or auth() == "ad_sec" or auth() == "ad_req") {
                ?>
                <div class="main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                        </div>
                        </div>
                        <div class="col-lg-8" style="border-right:dotted 0.3px;">
                            <?php echo $row['for_user']; ?>
                            <div id="resSearch" class="new_text" style="border-bottom: dotted 0.5px;">
                                <?php
                                $pdo = connectPDO();
                                function write($connect){
                                    $quantity = 10;
                                    $sql =  "SELECT * FROM infores";
                                    $result = $connect->query($sql . " " . "LIMIT $quantity ");
                                    while ($row = $result->fetch()) {
                                        $mail = $row['www_ir'];
                                        $text .= '<br><h3><p><a onclick="povna_infa(' . $row['id_ir']. ')"  target="_blank" style="font-weight: bold;">' . $row['title'] . '</a><br></p></h3><h4><p>' . 'Доменне імя: <a href =' . $mail . ' target="_blank" style="font-weight: bold;">' . $row['www_ir'] . '</a></p><p>' . 'Дата реєстрації: ' . $row['datare'] . '<p><span style="color:green;"> Активний </span></p>' . '</p><p>' . $row['owners'] . '</h4></p><hr>' . "\n";
                                    }
                                    echo $text;
                                }
                                write($pdo);
                                    ?>
                            </div>
                        </div>
                    <script>
                        function povna_infa(title) {
                            window.location.href='povna_infa.php?id=' + title + '' ;
                        }
                    </script>
                        <div class="col-lg-3">
                            <a style="margin-top: 20px;" href="#spoiler-1" data-toggle="collapse" class="btn btn-primary spoiler collapsed">Розширений пошук</a>
                            <div class="collapse" id="spoiler-1">
                                <div class="well">
                                    <script src="js/search.js"></script>
                                    <script src="js/jquery-3.2.1.js" type="text/javascript"></script>
                                    <form action="" name="myForm" id="myForm" method="post">
                                        <h4>Пошук інформації за реквізитом:</h4> <hr>
                                        <label title="Пошук по ключовим словам на сайті"><input type="checkbox" name="formDoor[]" value="1">Ключові слова</label><br>
                                        <label title="Пошук по даті реїстрації"><input type="checkbox" name="formDoor[]" value="2" >Дата реїстрації</label><br>
                                        <label title="Пошук по місту в якому підтримують цей сайт наприклад: Київ, Львів, Харків "><input type="checkbox" name="formDoor[]" value="3">Місце знаходження</label><br>
                                        <label title="Пошук по заголовку наприглад: Офіційни сайт Молоді"><input type="checkbox" name="formDoor[]" value="4">Тематична рубрика</label><br>
                                        <label title="Пошук по власнику сайту"><input type="checkbox" name="formDoor[]" value="5">Власник</label><br><hr>
                                        <h4>Пошук інформації за каталогом:</h4> <hr>
                                        <label title="Пошук за тематикою сайту, наприклад: наука, техніка, спорт."><input type="checkbox" name="formDoor[]" value="6">Тематична рубрика</label><br>
                                        <label title="Пошук за власниками сайту"><input type="checkbox" name="formDoor[]" value="7" >Власники</label><br>
                                        <label title="Пошук по регіонам розташування"><input type="checkbox" name="formDoor[]" value="8">Регіони</label><hr>
                                        <label title="Пошук по всім критеріям"><input type="checkbox" name=formDoor[] class=chk-all>Вибрати всі </label><br>
                                    </form>
                                    <!--            выбрать все          -->
                                    <script>
                                        $(document).on('change', 'input[type=checkbox]', function () {
                                            var $this = $(this), $chks = $(document.getElementsByName(this.name)), $all = $chks.filter(".chk-all");

                                            if ($this.hasClass('chk-all')) {
                                                $chks.prop('checked', $this.prop('checked'));
                                            } else switch ($chks.filter(":checked").length) {
                                                case +$all.prop('checked'):
                                                    $all.prop('checked', false).prop('indeterminate', false);
                                                    break;
                                                case $chks.length - !!$this.prop('checked'):
                                                    $all.prop('checked', true).prop('indeterminate', false);
                                                    break;
                                                default:
                                                    $all.prop('indeterminate', true);
                                            }
                                        });
                                    </script>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-2">
                            <div style="font-size: 14px;" class="select">
                                    <select>
                                        <?php
                                        if (($_SESSION['lang']=='ua')) {
                                            while ($row_categoris = $clasific->fetch()) {
                                                echo  '<option>' . $row_categoris['title'] . '</option>';
                                                }
                                                }
                                            if (($_SESSION['lang']=='en')){
                                                while ($row_categoris = $clasific->fetch()) {
                                                    echo '<option>' . $row_categoris['alias'] . '</option>';
                                                }
                                                }
                                                ?>
                                        </select>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        <?php
                    } else {
                        ?>
                        <div class="main">
                            <div class="container">
                                <div class="col-lg-8" style="border-right:dotted 0.3px;">
                                    <?php if ($_SESSION['lang']=='ua'){
                                        echo "<h2>Інформація для гостей ресурсу:</h2><br>
                                    <h4> Тільки зарегістровані користувачі мають доступ до перегляду ресурсів  <a href=\"auth/signup.php\" target=\"_blank\"> Регістрація</a> или  <a href=\"/auth/login.php\" target=\"_blank\"> Увійти</a>
                                    </h4><br>";
                                    }
                                    if ($_SESSION['lang']=='en'){
                                        echo "<h2>Information for guests of the resource:</h2><br>
                                    <h4>Only registered users have access to the resource view<a href=\"auth/signup.php\" target=\"_blank\"> Registration</a> or <a href=\"/auth/login.php\" target=\"_blank\"> Sing on</a>
                                    </h4><br>";
                                    }
                                    ?>
                                    <hr>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
?>



<?php include ("layouts/footer.php");

?>