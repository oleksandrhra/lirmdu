<?php include("doctype.php"); ?>
<?php include ('layouts/header.php'); ?>

<div class="header_bg"><!-- start header -->
	<div class="container-fluid">
        <div style="background-color: #F0F7E8" class="header row">
		<nav class="navbar" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Переключити навігацію</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php"></a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="menu nav navbar-nav ">
                  <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                  <li><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                  <li class="active"><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                  <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                  <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
		      </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group my_search">
                        <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		</div>
	</div>
</div>
<div class="main"><!-- start main -->
<div class="container">
	<div class="blog"><!-- start blog -->
		<div class="row">


			<div class="clearfix"></div>
		</div>
	</div><!-- end blog -->
</div>
</div>
<?php include ("layouts/footer.php");?>