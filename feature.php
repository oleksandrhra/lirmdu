<?php include ('doctype.php'); ?>
<?php include ("layouts/header.php");?>

<div class="header_bg"><!-- start header -->
    <div class="container-fluid">
        <div style="background-color: #F0F7E8" class="header row">
            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                            <span class="sr-only">Переключити навігацію</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="index.php"></a>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="menu nav navbar-nav ">
                            <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                            <li class="active"><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                            <li><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                            <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                            <li><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
                        </ul>
                        <form class="navbar-form navbar-right" role="search">
                            <div class="form-group my_search">
                                <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                            </div>
                        </form>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>

    </div>
</div>
<style>
    .content{
        background: #c1e2b3;
    }
    .center{
        background:  beige;
    }
    .title {
        text-shadow: 1px 1px 1px black;
        padding-bottom: 4px;
        padding-left: 4px;
        border-radius: 5px;
        background: #5F965F;
        background: rgb(95,150,95);
        background: -moz-linear-gradient(top, rgba(95,150,95,1) 0%, rgba(32,51,32,1) 100%);
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(95,150,95,1)), color-stop(100%,rgba(32,51,32,1)));
        background: -webkit-linear-gradient(top, rgba(95,150,95,1) 0%,rgba(32,51,32,1) 100%);
        background: -o-linear-gradient(top, rgba(95,150,95,1) 0%,rgba(32,51,32,1) 100%);
        background: -ms-linear-gradient(top, rgba(95,150,95,1) 0%,rgba(32,51,32,1) 100%);
        background: linear-gradient(to bottom, rgba(95,150,95,1) 0%,rgba(32,51,32,1) 100%);

    }
    .news {
        font-family: Verdana, sans-serif;
        font-size: 17px;
        color: black;
        text-align: justify;
        background: beige;
    }
</style>
<div class="main content"><!-- start main -->
<div class="container center ">
	<div class="features "><!-- start feature -->
		<div class="new_text">
            <h4 class="title">04.09.2015 19:53</h4>
                <div class="news ">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.<br>
                    Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.<br></div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
            <h4 class="title">04.09.2015 19:53</h4>
            <div class="news">Проблемы с авторизацией из-за нового релиза API от Варгейминга. Сегодня ночью будет исправлено.</div>
           	</div><!-- end feature -->
</div>
</div>
<?php include ("layouts/footer.php")?>