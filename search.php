<?php
include ("public/connect.php");


///////////// Сам скрипт обработчик ///////////////
if (isset ($_POST['search'])) {
    $connect = connectPDO();
    // Открываем соединение с базой данных
    search($_POST['search'],$connect);
    // Закрываем соединение с  базой данных
}


function search($search, $mysqli)
{
//Поисковый запрос не пустой?
    if (!empty($search)) {
        $search = htmlspecialchars($search);//убераем пробелы
        $search = stripslashes($search);//нижний регистр
        $search = addslashes($search);//Убираем эти строки иначе не ищет с
        if (strlen($search) < 4) {
            $text = 'короткий пошуковий запит';
        } elseif (strlen($search) > 50) {
            $text = 'довгий пошуковий запит';
        } else {
            $aDoor = $_POST['checkedValues'];
            if (empty($aDoor) || count($aDoor) === 0) {
                       $sql = "SELECT * FROM infores WHERE  title LIKE :titles OR www_ir LIKE :wwwir  OR mithe_znahod LIKE :mithe_znahod  OR datare LIKE :datare  OR owner_ir LIKE :ownerir  OR lang_ir LIKE :langir OR title_classif LIKE :titleclassif  OR region LIKE :region OR  key_words LIKE :keywords OR id_ir LIKE :id_ir OR owners LIKE :owners";
                    for ($i = 0; $i < 12; $i++) {
                        $sql_array[$i] = true;
                    }
            } else {
                $N = count($aDoor);
                $sql = "SELECT * FROM infores WHERE id_ir LIKE :id_ir";
                $sql_array[0] = true;
                for ($i = 0; $i < $N; $i++) {
                    switch ($aDoor[$i]) {
                        case 1:
                            $sql = "$sql OR key_words LIKE :keywords";
                            $sql_array[1] = true;
                            break;
                        case 2:
                            $sql = "$sql OR datare LIKE :datare";
                            $sql_array[2] = true;
                            break;
                        case 3:
                            $sql = "$sql OR mithe_znahod LIKE :mithe_znahod";
                            $sql_array[3] = true;
                            break;
                        case 4:
                            $sql = "$sql OR title LIKE :titles";
                            $sql_array[4] = true;
                            break;
                        case 5:
                            $sql = "$sql OR owner_ir LIKE :ownerir";
                            $sql_array[5] = true;
                            break;
                        case 6:
                            $sql = "$sql OR title_classif LIKE :titleclassif";
                            $sql_array[6] = true;
                            break;
                        case 7:
                            $sql = "$sql OR owners LIKE :owners";
                            $sql_array[7] = true;
                            break;
                        case 8:
                            $sql = "$sql OR region LIKE :region";
                            $sql_array[8] = true;
                            break;
                    }
                }
            }
            // Устанавливаем количество записей, которые будут выводиться на одной странице
// Поставьте нужное вам число. Для примера я указал одну запись на страницу
            $quantity = 10;

// Ограничиваем количество ссылок, которые будут выводиться перед и
// после текущей страницы
            $limit = 5;

            $page = $_POST['page'];

// Если значение page= не является числом, то показываем
// пользователю первую страницу
            if (!is_numeric($page)) $page = 1;

// Если пользователь вручную поменяет в адресной строке значение page= на нуль,
// то мы определим это и поменяем на единицу, то-есть отправим на первую
// страницу, чтобы избежать ошибки
            if ($page < 1) $page = 1;
// Узнаем количество всех доступных записей
            $mysqli = connectPDO();
            $statement = $mysqli->prepare($sql);
                if ($sql_array[0] == true)
            $statement->bindParam(':id_ir', $id_ir); $id_ir = "%$search%";
                if ($sql_array[1] == true)
            $statement->bindParam(':keywords', $keywords); $keywords = "%$search%";
                if ($sql_array[2] == true)
            $statement->bindParam(':datare', $datare); $datare = "%$search%";
                if ($sql_array[3] == true)
            $statement->bindParam(':mithe_znahod', $mithe_znahod); $mithe_znahod = "%$search%";
                if ($sql_array[4] == true)
            $statement->bindParam(':titles', $titles); $titles = "%$search%";
                if ($sql_array[5] == true)
            $statement->bindParam(':ownerir', $ownerir); $ownerir = "%$search%";
                if ($sql_array[6] == true)
            $statement->bindParam(':titleclassif', $titleclassif); $titleclassif = "%$search%";
                if ($sql_array[7] == true)
            $statement->bindParam(':owners', $owners); $owners = "%$search%";
                if ($sql_array[8] == true)
            $statement->bindParam(':region', $region); $region = "%$search%";
                if ($sql_array[9] == true)
            $statement->bindParam(':wwwir', $wwwir); $wwwir = "%$search%";
                if ($sql_array[10] == true)
            $statement->bindParam(':mithe_znahod', $mithe_znahod); $mithe_znahod = "%$search%";
                if ($sql_array[11] == true)
            $statement->bindParam(':langir', $langir); $langir = "%$search%";
            $statement->execute();
            $num = $statement->rowCount();



// Вычисляем количество страниц, чтобы знать сколько ссылок выводить
            $pages = $num / $quantity;
            $pages = ceil($pages);

// Здесь мы увеличиваем число страниц на единицу чтобы начальное значение было
// равно единице, а не нулю. Значение page= будет
// совпадать с цифрой в ссылке, которую будут видеть посетители
            $pages++;

// Если значение page= больше числа страниц, то выводим первую страницу
            if ($page > $pages) $page = 1;

// Выводим заголовок с номером текущей страницы 
            /*echo '<h2><strong style="color: #df0000">Сторінка № ' . $page .
                '</strong></h2><br /><br />';*/

// Переменная $list указывает с какой записи начинать выводить данные.
// Если это число не определено, то будем выводить
// с самого начала, то-есть с нулевой записи
            if (!isset($list)) $list = 0;

// Чтобы у нас значение page= в адресе ссылки совпадало с номером
// страницы мы будем его увеличивать на единицу при выводе ссылок, а
// здесь наоборот уменьшаем чтобы ничего не нарушить.
            $list = --$page * $quantity;

// Делаем запрос подставляя значения переменных $quantity и $list
            $statement = $mysqli->prepare($sql . " " . "LIMIT $quantity OFFSET $list;");
            if ($sql_array[0] == true)
                $statement->bindParam(':id_ir', $id_ir); $id_ir = "%$search%";
            if ($sql_array[1] == true)
                $statement->bindParam(':keywords', $keywords); $keywords = "%$search%";
            if ($sql_array[2] == true)
                $statement->bindParam(':datare', $datare); $datare = "%$search%";
            if ($sql_array[3] == true)
                $statement->bindParam(':mithe_znahod', $mithe_znahod); $mithe_znahod = "%$search%";
            if ($sql_array[4] == true)
                $statement->bindParam(':titles', $titles); $titles = "%$search%";
            if ($sql_array[5] == true)
                $statement->bindParam(':ownerir', $ownerir); $ownerir = "%$search%";
            if ($sql_array[6] == true)
                $statement->bindParam(':titleclassif', $titleclassif); $titleclassif = "%$search%";
            if ($sql_array[7] == true)
                $statement->bindParam(':owners', $owners); $owners = "%$search%";
            if ($sql_array[8] == true)
                $statement->bindParam(':region', $region); $region = "%$search%";
            if ($sql_array[9] == true)
                $statement->bindParam(':wwwir', $wwwir); $wwwir = "%$search%";
            if ($sql_array[10] == true)
                $statement->bindParam(':mithe_znahod', $mithe_znahod); $mithe_znahod = "%$search%";
            if ($sql_array[11] == true)
                $statement->bindParam(':langir', $langir); $langir = "%$search%";
            $statement->execute();
// Выводим все записи текущей страницы
            echo '<h3>За вашим запитом знайдено <span style="color:green;">' . $num . '</span> результатів</h3>';
            add_page($page, $limit, $pages);
            if ($statement->rowCount() > 0) {
                while($row = $statement ->fetch()){
                    $text .= '<br><h3><a onclick="povna_infa(' . $row['id_ir']. ');"  target="_blank" style="font-weight: bold;">' . $row['title'] . '</a></h3><h4>' . 'Доменне імя: <a href =' . $row['www_ir']. ' target="_blank" style="font-weight: bold;">' . $row['www_ir'] . '</a></p><p>' . 'Дата реєстрації: ' . $row['datare'] . '<p><span style="color:green;"> Активний </span></p>' . '</p><p>' . $row['owners'] . '</p></h4><hr>' . "\n";
            }
                $bold = '<span style="color:red;">' . $search . '</span>';
                $text = str_ireplace($search, $bold, $text);
            } else {
                $text = 'По запиту нічого не знайдено.';
            }
        }
    } else {
        $text = 'Задан пустий запит.';
    }
//Возвращаем сформированную строку поисковой выдачи
    echo $text;
    add_page($page, $limit, $pages);
}




function add_page ($page, $limit, $pages){
    // _________________ начало блока 1 _________________
echo '<ul class="pagination">';
// Выводим ссылки "назад" и "на первую страницу"
    if ($page >= 1) {

        // Значение page= для первой страницы всегда равно единице,
        // поэтому так и пишем
        echo '<li><a onclick="changePage(' . 1 . ')" href="#">' . '<<' . '</a></li> &nbsp; ';

        // Так как мы количество страниц до этого уменьшили на единицу,
        // то для того, чтобы попасть на предыдущую страницу,
        // нам не нужно ничего вычислять
        echo '<li><a onclick="changePage(' . $page . ')" href="#">' . '<' . '</a></li> &nbsp; ';
    }

// __________________ конец блока 1 __________________

// На данном этапе номер текущей страницы = $page+1
    $thiss = $page + 1;

// Узнаем с какой ссылки начинать вывод
    $start = $thiss - $limit;

// Узнаем номер последней ссылки для вывода
    $end = $thiss + $limit;

// Выводим ссылки на все страницы
// Начальное число $j в нашем случае должно равнятся единице, а не нулю
    for ($j = 1; $j < $pages; $j++) {

        // Выводим ссылки только в том случае, если их номер больше или равен
        // начальному значению, и меньше или равен конечному значению
        if ($j >= $start && $j <= $end) {
            // Ссылка на текущую страницу выделяется жирным
            if ($j == ($page + 1)) echo '<li><a  href="#"><strong style="color: #df0000">' . $j .
                '</strong></a></li> &nbsp; ';
            // Ссылки на остальные страницы
            else echo '<li><a onclick="changePage(' . $j . ')" href="#">' . $j . '</a></li> &nbsp; ';
        }
    }
// _________________ начало блока 2 _________________

// Выводим ссылки "вперед" и "на последнюю страницу"
    if ($j >2)
    if ($j > $page && ($page + 1) < $j) {

        // Чтобы попасть на следующую страницу нужно увеличить $pages на 2
        if($page != $pages-2) {
            echo '<li><a onclick="changePage(' . ($page + 2) . ')" href="#">' . '>' . '</a></li> &nbsp; ';
            // Так как у нас $j = количество страниц + 1, то теперь
            // уменьшаем его на единицу и получаем ссылку на последнюю страницу
            echo '<li><a onclick="changePage(' . ($j - 1) . ')" href="#">' . '>>' . '</a></li> &nbsp; ';
        }
    }
    echo '</ul>';
}
?>