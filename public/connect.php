<?php
function connectDB (){
// Определяем константы для соединения с базой данных
    define('DB_HOST', 'localhost');
    define('DB_USER', 'root');
    define('DB_PASS', '');
    define('DB_NAME', 'lirmdunew');

//Пытаемся соединится с базой данных
    $mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
    if ($mysqli->connect_errno) {
        printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
        exit();
    }
// Устанавливаем кодировку
//Возвращаем дескриптор соединения
    return $mysqli;
}

/*Закрываем соединение с базой данных*/
function closeDB($mysqli){
    mysqli_close($mysqli);
}

function connectPDO (){
    $host = '127.0.0.1';
    $db   = 'lirmdunew';
    $user = 'root';
    $pass = '';
    $charset = 'utf8';

    $dsn = "mysql:host=$host;dbname=$db;charset=$charset";
    $opt = [
        PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        PDO::ATTR_EMULATE_PREPARES   => false,
    ];
    $pdo = new PDO($dsn, $user, $pass, $opt);
    return $pdo;
}

?>