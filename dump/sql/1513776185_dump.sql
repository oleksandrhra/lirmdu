-- SQL Dump
-- my_ version: 1.234
--
-- База дынных: `lirmdunew`
--
-- ---------------------------------------------------
-- ---------------------------------------------------

-- 
-- Структура таблицы - admin_panel
--
DROP TABLE IF EXISTS `admin_panel`;
CREATE TABLE `admin_panel` (
  `admin_button` varchar(30) NOT NULL,
  `enter_button` varchar(20) NOT NULL,
  `exit_button` varchar(20) NOT NULL,
  `registr_button` varchar(20) NOT NULL,
  `search_button` varchar(20) NOT NULL,
  `title_button` varchar(20) NOT NULL,
  `news_button` varchar(20) NOT NULL,
  `contact_button` varchar(20) NOT NULL,
  `ir_button` varchar(20) NOT NULL,
  `about_button` varchar(20) NOT NULL,
  `footer` text NOT NULL,
  `for_user` varchar(500) NOT NULL,
  `slayder` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :admin_panel
--
			
INSERT INTO `admin_panel` VALUES("Адміністративна панель","Увійти","Вийти","Зареєструватися","Пошук","Головна","Новини","Контакти","Інформаційний реєстр","Про Реєстр","<p><span>© Інформаційний реєстр даних Міністерства Оборони України, 2013-2017</span></p>\r\n<p>Розроблено та підтримується Військовим інститутом телекомунікацій та інформатизацій</p>            ","<h2>Інформація для власників ресурсів:</h2><br>\r\n                            <h4>Якщо ви є власником ресурсу та маєте бажання занести цей ресурс у Національний реєстр електронних інформаційних ресурсів - будь ласка, скористайтесь інструкцією на цій <a href = \"reristrion_page.php\" target=\"_blank\"> сторінці.</a></h4><br><hr>","<h1 style=\"color: black;\">Реєстраційний портал</h1>\r\n                <h4 style=\"color:black\">Електронна державна реєстрація юридичних осіб та фізичних осіб-підприємців</h4>\r\n                <p style=\"font-size: 14px; color: black\">Можливість подання державному реєстратору електронних документів для проведення державної реєстрації юридичної особи або фізичної особи-підприємця у Єдиному державному реєстрі.<br><br><br><br>\r\n                    Детальніше</p>                ");

-- 
-- Структура таблицы - admin_panelen
--
DROP TABLE IF EXISTS `admin_panelen`;
CREATE TABLE `admin_panelen` (
  `admin_button` varchar(30) NOT NULL,
  `enter_button` varchar(30) NOT NULL,
  `registr_button` varchar(30) NOT NULL,
  `exit_button` varchar(30) NOT NULL,
  `search_button` varchar(30) NOT NULL,
  `title_button` varchar(30) NOT NULL,
  `news_button` varchar(30) NOT NULL,
  `ir_button` varchar(30) NOT NULL,
  `about_button` varchar(30) NOT NULL,
  `contact_button` varchar(30) NOT NULL,
  `footer` varchar(500) NOT NULL,
  `for_user` text NOT NULL,
  `slayder` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :admin_panelen
--
			
INSERT INTO `admin_panelen` VALUES("Admin panel"," Sign in"," Registration","Go out","Search","Home","News","Information register","About the Register","Contacts","<p><span>© \r\nInformation register of data of the Ministry of Defense of Ukraine, 2013-2017</span></p>\r\n<p>Developed and maintained by the Military Institute for Telecommunications and Informatics </p>                        ","<h2>Information for resource owners:</h2><br>\r\n<h4>f you are the owner of a resource and you wish to include this resource in the National Register of Electronic Information Resources - please use the instructions on  <a href=\"reristrion_page.php\" target=\"_blank\"> this page.</a></h4><br><hr>                           ","<h1 style=\"color: black;\">\r\nRegistration portal</h1>\r\n<h4 style=\"color:black\">\r\nElectronic state registration of legal entities and individual entrepreneurs</h4>\r\n<p style=\"font-size: 14px; color: black\">Possibility of submitting to the state registrar electronic documents for the state registration of a legal entity or an individual entrepreneur in the Unified State Register.<br><br><br><br>\r\n                    Read more</p>");

-- 
-- Структура таблицы - categories
--
DROP TABLE IF EXISTS `categories`;
CREATE TABLE `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` int(11) NOT NULL DEFAULT '0',
  `alias` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_alias_unique` (`alias`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
--			
-- Dump BD - tables :categories
--
			
INSERT INTO `categories` VALUES("1","Класифікатор","0","Classifier","",""),("2","Інспектування військ(сил)","1","Inspection of troops","2016-07-18 00:00:00",""),("3","Питання внутрішнього аудиту","1","The issue of internal audit","2016-07-18 00:00:00",""),("4","Запобігання та виявлення корупції","1","Preventing and Detecting Corruption","2016-07-18 00:00:00",""),("5","Законопроектна та нормотворча робота","1","Draft law and normative work","",""),("15","Правове забезпечення","1","Legal support","",""),("16","Претензійна та позовна робота","1","Claim and litigation work","",""),("17","Фінансове забезпечення Збройних Сил України","1","Financial support of the Armed Forces of Ukraine","",""),("26","Фінансово-господарська діяльність держави підприємств МОУ","1","Financial and economic activity of the state of the enterprises of the Ministry of Defense of Ukraine","",""),("27","Кадрова політика","1","Personnel policy","",""),("28","Підготовка кадрів","1","Training of personnel","",""),("29","Діяльність військових представництв МОУ","1","Activities of Military Missions of the Ministry of Defense","",""),("30","Воєнна політика та стратигічне планування","1","Military Policy Strategic Planning","",""),("31","Міжнародне співробітництво","1","international cooperation","",""),("32","Контроль за озброєнням","1","Arms control","",""),("33","Військово-технічне співробітництво та експортний контроль","1","Military-technical cooperation and export control","",""),("34","Миротворча діяльність","1","Peacekeeping activities","",""),("35","Співробітництво з НАТО, асоціація Україна-ЄС","1","Cooperation with NATO, EU-Ukraine Association","",""),("36","ГУманітарна та соціальна політика","1","Humanitarian and social policy","",""),("37","Військова освіта","1","Military education","",""),("38","Наукова, науково-технічна та інноваційна діяльність, НДДКР в інтересах виконання завдання завдань оборони","1","Scientific, scientific and technical and innovation activity, R & D in the interests of the task of defense tasks","",""),("39","Перепідготовка та підвищення кваліфікації військових фахівців, державних службовців та іноземних громадян","1","Retraining and advanced training of military specialists, civil servants and foreign citizens","",""),("40","Закупівля і модернізація озброєння та військової техніки ЗСУ","1","Purchase and modernization of armament and military equipment of the Armed Forces","",""),("41","Нові зразки ОВТ та прийняті на озброєння","0","New models of weapons and equipment adopted","",""),("52","Нові зразки речового майна, норми продовольчого та речового забезпечення","1","New samples of real estate, food and property standards","",""),("53","База даних виробників та постачальників предметів речового майна, продуктів харчування а також матеріально-технічних засобів речової  та продовольчої служб","1","Database of producers and suppliers of real estate, food products, as well as material and technical facilities of the real and food services","",""),("54","Реформування системи стандартизації у сфері оборони","1","Reform of the system of standardization in the field of defense","",""),("55","Впровадження  та застосування стандартів НАТО на підприємствах оборонно-промислового комплексу України та ЗСУ","1","Implementation and application of NATO standards at enterprises of the defense industrial complex of Ukraine and the Armed Forces of Ukraine","",""),("56","Інформаційна діяльність т взаємодія із засобами масової інформації","1","Information activities t interact with mass media","",""),("57","Фізична підготовка і спорт у Збройних СИлах України","1","Physical Training and Sports in the Armed Forces of Ukraine","",""),("58","Нормативно-правове забезпечення фізичної підготовки і спорту у Збройних силах Ураїни","1","Normative legal support of physical training and sports in the Armed Forces of Ukraine","",""),("59","Квартирний облік та забезпечення житлом військовослужбовців і членів їх сімей","1","Housing accounting and provision of housing for servicemen and their families","",""),("60","Облік нерухомого майна","1","Real estate accounting","",""),("61","Облік земель оборони","1","Land defense inventory","",""),("62","Укомплектованість Збройних Сил України основними видами ОВТ, стан технічної готовності. Технічне забезпечення","1","Availability of the Armed Forces of Ukraine by the main types of equipment, state of tec","",""),("63","Медичне забезпечення","1","Medical support","",""),("64","Державні закупівлі. Конкурсні торги","1","Government Procurement. Competitive bidding","",""),("65","Публічні закупівлі за державні кошти","1","Public Procurement for Public Funds","",""),("66","Ресурсне (тилове) забезпечення","1","Resource (back) support","",""),("67","Звільнення Збройних Сил України від надлишкового майна, у тому числі нерухомого, та його реалізація","1","Dismissal of the Armed Forces of Ukraine from excess property, including real estate, and its realization","",""),("68","Утилізація списаного та непридатного для використання за призначенням військового майна(у тому числі ОВТ, компонентів рідкого ракетного палива та інших токсичних речовин, боєприпасів, ракет, що зберігаюсться у Збройних Силах України)","1","Utilization of the disposable and unsuitable for use in the use of military property (including weapons, components of liquid rocket fuel and other toxic substances, ammunition, missiles stored in the Armed Forces of Ukraine)","",""),("69","Облік надлишкового військового майна (крім стрылецькоъ зброъ, боєприпасів та нерухомого майна)","1","Accounting for surplus military assets (Crimean sniper weapons, ammunition and real estate)","",""),("70","Виконавча дисципліна. Погодження проектів нормативно-правових актів в Центральних органахвиконавчої влади, Адміністрації президента Укаїни, Кабінета Міністрів України, Верховній Раді України","1","Executive discipline. Approval of draft regulations in the central organs of executive power, the Administration of the President of Ukraine, the Cabinet of Ministers of Ukraine, the Verkhovna Rada of Ukraine","",""),("71","Робота з громадянами та доступ до публічної інформації","1","Work with citizens and access to public information","",""),("82","Розгляд звернень громадян","1","Consideration of appeals of citizens","",""),("83","Охорона державної таємниці","1","Protection of state secrets","",""),("84","Забезпечення технічного захисту інформації","1","Providing technical protection of information","",""),("85","Мобілізаційна підготовка","1","Mobilization training","",""),("86","Державний технічний нагляд","1","State technical supervision","",""),("87","Впровадження інформаційно-аналітичної системи управління Міністерства Оборони України та Генерального штабу Збройних Сил України","1","Implementation of the information and analytical system of the Ministry of Defense of Ukraine and the General Staff of the Armed Forces of Ukraine","",""),("88","Впровадження сучасних інформаційних технологій у сфері оборони","1","Implementation of modern information technologies in the field of defense","",""),("89","Забезпечення інформаційної безпеки Міністерства Оборони України","1","Providing information security to the Ministry of Defense of Ukraine","",""),("90","Забезпечення зв\'язку та інформаційних систем","1","Providing communication and information systems","",""),("91","Забезпечення кібернетичної безпеки в інформаційно-телекомунікаційних системах","1","Providing cybernetic security in information and telecommunication systems","",""),("112","Забезпечення кібернетичної безпеки в інформаційно-телекомунікаційних системах","1","Providing cybernetic security in information and telecommunication systems\r\n","",""),("113","ПРотимінна діяльність. Екологічна безпека. Цивільний захист","1","\r\n59/5000\r\nPRotyminna diyalʹnistʹ. Ekolohichna bezpeka. Tsyvilʹnyy zakhyst\r\nPrimary activity. Ecological safety. Civil Protection","",""),("114","Регулювання діяльності державної авіації України","1","Regulation of State Aviation Activities of Ukraine","",""),("115","Архівна діяльність","1","Archive activity","",""),("116","Забезпечення службової діяльності Міністерства оборони та Генерального штабу Збройних Сил України","1","Maintenance of official activities of the Ministry of Defense and the General Staff of the Armed Forces of Ukraine","",""),("117","Діяльність Сухопутних військ Збройних Сил України","1","Activities of the Land Forces of the Armed Forces of Ukraine","",""),("118","Діяльність повітряних Сил Збройних сил України","1","Activities of the Air Forces of the Armed Forces of Ukraine","",""),("119","Діяльність Військово-Морських Сил Збройних Сил України","1","Activities of the Naval Forces of the Armed Forces of Ukraine","",""),("120","Діяльність Сил спеціальних операцій Збройних Сил України","1","Activities of the Special Operations Forces of the Armed Forces of Ukraine","",""),("122","Діяльність високомобільних десантних військ Збройних Сил України","1","Activities of high-level landing forces of the Armed Forces of Ukraine","",""),("123","Підготовка Збройних Сил України","1","Training of the Armed Forces of Ukraine","",""),("124","Забезпечення правопорядку","1","Enforcement of law and order","","");

-- 
-- Структура таблицы - contact
--
DROP TABLE IF EXISTS `contact`;
CREATE TABLE `contact` (
  `id_contact` int(11) NOT NULL AUTO_INCREMENT,
  `name_c` varchar(20) NOT NULL,
  `email_c` varchar(40) NOT NULL,
  `tel_c` varchar(16) NOT NULL,
  `question` text NOT NULL,
  PRIMARY KEY (`id_contact`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :contact
--
			
INSERT INTO `contact` VALUES("1","asfasgasgas","asfasga","safasf","asgasgasg"),("2","","",""," "),("3","agasg","oleksandr@hra.com","957958458357"," asgghWHARHERH");

-- 
-- Структура таблицы - infores
--
DROP TABLE IF EXISTS `infores`;
CREATE TABLE `infores` (
  `id_ir` int(11) NOT NULL AUTO_INCREMENT,
  `id_classif` int(11) NOT NULL,
  `id_page_info` int(11) NOT NULL,
  `key_words` varchar(200) NOT NULL,
  `datare` varchar(10) NOT NULL,
  `mithe_znahod` varchar(100) NOT NULL,
  `title` varchar(200) NOT NULL,
  `owner_ir` varchar(100) NOT NULL,
  `lang_ir` varchar(100) NOT NULL,
  `www_ir` varchar(100) NOT NULL,
  `phone_number` varchar(100) NOT NULL,
  `title_classif` varchar(200) NOT NULL,
  `owners` varchar(200) NOT NULL,
  `region` text NOT NULL,
  PRIMARY KEY (`id_ir`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :infores
--
			
INSERT INTO `infores` VALUES("1","1","1","фондовий ринок, інформаційне висвітлення, фондовий світ, цінні папери, учасники ринку, інвестиції","2017-10-02","Україна, Київ, вул.Олени Теліги,буд.41. ","Stockworld","Керівник: Директор Лиходід Сергій Олександрович","українська, англійська","http://www.stockworld.com.ua","+380635054404","наука","Товариство з обмеженою відповідальністю \"Фондовий світ\"","Київська область"),("2","1","1","наука, економіка, інновації, інвестиції, сталий розвиток, нанотехнології, технопарки","2017-10-09","м.Київ бул.Л.Українки 30а-9","Науково-практичний журнал \"Проблеми інноваційно-інвестиційного розвитку\"","Керівник: Головний редактор професор Шморгун Леонід Григорович","українська, англійська","http://nonproblem.com","044 362-2822","література","Студія \"n1n3.net\"","Київська область"),("3","1","1","теплицька рада","2017-08-08","Вінницька обл. смт.Теплик вул.Незалежності,25 ","Веб-сайт Теплицької районної державної адміністрації","Керівник: Голова Теплицької районної державної адміністрації Романюк Василь Іванович","українська, англійська","http://www.teplykrda.gov.ua","043-532-14-63","політика","Теплицька районна державна адміністрація","Вінницька область"),("4","1","1","Національна гвардія України, Внутрішні війська України","2017-10-16","м. Київ вул.Микитинська 69б","Офіційний сайт Національної гвардії України","Керівник: Командувач Національної гвардії України, Полторак Степан Тимофійович","українська, англійська","http://www.vv.gov.ua","+380983456786","служба","Національна академія НГУ","Київська область"),("5","1","1","інспекційна діяльність, екологія","2017-06-07","Чернігів, бульв. Капустин 45","Інформаційна система для організації інспекції","Керівник: Казимир Михайло Миколайович","українська, англійська","http://dei.gov.ua","+380783451234","діяльність","Державна екологічна інспекція України","Чернігівська область"),("6","1","1","бізнес новини, графіки перевірок, кридитування бізнесу, нормативні акти, законодавство, тренінг, семінар","2017-10-27","Вінниця, вул.Китайська 67","Бізнес-Дайджест","Керівник: Голва правління Печалін Олександр Валентинович","українська, англійська","http://digest.stina.org.ua","+380785436545","бізнес","Вінницька обласна громадська організація Спілка підприємців СТІНА","Вінницька область"),("7","1","1","українське класичне кіно","2017-10-10","Харків, вул.Шалова 34а","Інтернет портал Українського класичного кіно","Керівник: Перший заступник Голови Сьомкін С.В.","українська, англійська","http://ukrainianclassics.com","+30923457896","культура","ТОВ Нова Європейська Водплатформа","Харківська область"),("8","1","1","райдержадміністрація, виноградівська рда","2017-10-03","Виноградар, вул.Каштанова 45","ЄДКС Карта","Керівник: В.о. голови районної державної адміністрації Русанюк Михайло Михайлович","українська, англійська","http://vynogradiv-rda.gov.ua","+380446789898","політика","Виноградівська районна державна адміністрація","Київська область"),("9","1","1","райдержадміністрація, виноградівська рада, спорт, молодь","2017-03-13","Виноградар, вул.Каштанова 49","Управління освіти,молоді та спорту Виноградівської районної державна адміністрація","Керівник: Начальник управління Белень Василь Іванович","українська, англійська","http://rajvosevlush.net.ua","+380675643456","культура","Управління освіти,молоді та спорту Виноградівської районної державна адміністрація","Київська область"),("10","1","1","офіційна веб-сторінка, закарпатська обласна державна адміністрація","2017-10-08","Закарпаття","Офіційна веб-сторінка Закарпатської обласної державної адміністрації","Керівник: Голова облдержадміністрація Лунченко Валерій Валерійович","українська, англійська","http://carpathia.gov.ua","+380978764543","політика","Закарпатська обласна державна адміністрація,","Закарпатська область"),("11","1","1","","2016-06-12","Кіровоград, бульв. Родинний 78г","Офіційний веб-сайт служби у справах дітей Кіровоградської обласної державної адміністрації","Керівник: Заступник начальника служби Чернявська Наталія Сергіївна","українська, англійська","http://dity.kr-admin.gov.ua","+380445890706","політика","Служба у справах дітей Кіровоградської обласної державні адміністрації","Кіровоградська область"),("12","1","1","управління культури і туризму вінницької обласної державної адміністрації","2017-10-03","Вінниця, вул. Зокова 67","Веб-сайт Управління культури і туризму Вінницької обласної державної адміністрації","Керівник: Начальник управління Скрипник Марія Василівна","українська, англійська","http://vincult.org.ua","+380445631312","культура","Управління культури і туризму Вінницької обласної державної адміністрації","Вінницька область"),("13","1","1","медицина, патологічна анатомія мова українська, російська, англійська формат електронніпублікації","2017-10-02","Дніпропетровськ, вул. Волкова 73","Науковий електронний фаховий журнал Морфології","Керівник: Ректор, Дзяк Георгій Вікторов","українська, англійська","http://morphology.dp.ua","+380449089898","наука","Державний заклад Дніпропетровська медична академія Міністерства охорони здоров\'я України","Дніпропетровська область"),("14","1","1","департамент соціальної політики, Вінниця","2016-08-22","Вінниця, вул. Жукова 56","Веб-сайт Департаменту соціальної політики Вінницької обласної державної адміністрації","Керівник: Директор Департаменту Коваль М.Є.","українська, англійська","http://www.socinform.vn.ua","+380443215678","політика","Департамент соціальної політики Вінницької обласної державної адміністрації","Вінницька область"),("15","1","1","департамент екології та природних ресурсів кіровоградської обласної державної адміністраці","2017-10-11","кіровоград, вул. Локова 67","Офіційний веб-сайт департаменту екології та природних ресурсів облдержадміністрації","Керівник: Директор департаменту екології та природних ресурсів облдержадміністрації Ковтунов Олексан","українська, англійська","http://ecolog.kr-admin.gov.ua","+380441234567","екологія","Департамент екології та природних ресурсів Кіровоградської обласної державної адміністрації","Кіровоградська область"),("16","1","1","управління комунікацій з громадськістью кіроваградської облдержадміністрації","2017-10-04","Кіровоград, вуд.Лукова 45д","Офіційний веб-сайт Управління комунікацій з громадськістью Кіроваградської облдержадміністрації","Керівник: Начальник управління Останній Віталій Миколайович","українська, англійська","http://uvp.kr-admin.gov.ua","+380446789567","спільнота","Управління комунікацій з громадськістю Кіровоградської обласної державної адміністрації","Кіровоградська область"),("17","1","1","містобудування, архітектура, регіональний розвиток","2017-10-01","Кіровоград, вул. Шикова 6","Офіційний веб-сайт департаменту регіонального розвитку, містобудування та архітектури Кіровоградської обласної державної адміністрації","Керівник: Директор департаменту Кулікова В.В.","українська, англійська","http://oblarch.kr-admin.gov.ua","+380448976908","архітектура","Департамент регіонального розвитку, містобудування та архітектури Кіровоградської обласної адміністрації","Кіровоградська область"),("18","1","1","Міністерство оборони України, збройні сили україни","2017-10-04","Київ, вул. Вокзальна 7","Офіційний веб-портал Міністерства оборони України","Керівник: Тенюх Ігор Йсопович","українська, англійська","http://mil.gov.ua","+38044796789","структури","Військова частина А0351","Київська область"),("19","1","1","освітянськи новини тернопільщини","2017-10-28","Тернопіль, вул Дорова 4","Офіційний сайт Департаменту освіти і науки Тернопільської обласної державної адміністрації"," Керівник: Директор департаменту Запорожан І.Г.","українська, англійська","http://osvita.te.gov.ua","+380441231212","освіта","Департамент освіти і науки ТОДА","Тернопільська область"),("20","1","1","олександрівська рда, олександрівська районна державна адміністрація, олександрівська райдержадміністрація","2017-06-11","Олександрівка, вул. Шолкова 76","Офіційний веб-сайт Олександрівської районної державної адміністрації","Керівник: Голова Олександрівської районної державної адміністрації Авксентьєв Валерій Володимирович","українська, англійська","http://olexrda.kr-admin.gov.ua","+380447890808","заклади","Олександрівська районна державна адміністрація Кіровоградської області","Олександрівська область");

-- 
-- Структура таблицы - lang_ir
--
DROP TABLE IF EXISTS `lang_ir`;
CREATE TABLE `lang_ir` (
  `en_lang` varchar(10) NOT NULL,
  `ua_lang` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :lang_ir
--
			
INSERT INTO `lang_ir` VALUES("en","ua");

-- 
-- Структура таблицы - news
--
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news` (
  `id_news` int(4) NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `news` text NOT NULL,
  PRIMARY KEY (`id_news`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
--			
-- Dump BD - tables :news
--
			
INSERT INTO `news` VALUES("1","2017-12-14","Перешли на новый вид таблеток"),("2","2017-12-14","уничтожили инопланетян"),("5","2017-12-14","Перешли на новый вид таблеток");

-- 
-- Структура таблицы - user
--
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `login` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `surname` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tel` double DEFAULT NULL,
  `reg_time` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `law` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
--			
-- Dump BD - tables :user
--
			
INSERT INTO `user` VALUES("2","BeSaMe","4d9478288b78bb632bd394849500c9f6296264aee87dba41e585d96b4f53cb79","Oleksandr","Khraban","Oleksandrhra@gmail.com","380937163487","Tue, 12 Dec 2017 16:17:37 +0200","user");
