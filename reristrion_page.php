<?php include('doctype.php'); ?>
<?php include ('layouts/header.php'); ?>
<form action="" method="POST" onsubmit="return checkform(this);">
    <input name="action" value="generate_pdf" type="hidden">
    <input name="entity" value="resources" type="hidden">
    <style type="text/css">
        .app {
            background-color: transparent;
            font-family: 'Courier New';
            font-size: 10.5pt;
            color: black;
            width: 700px;
            margin: auto;
        }
        .app_input {
            border: 0 none white;
            border-bottom: solid black 1px;
            background-color: transparent;
            font-family: 'Courier New';
            font-size: 10.5pt;
            color: black;
        }
        .app_input_invalid {
            border: 0 none white;
            border-bottom: solid black 1px;
            background-color: #fdd;
            border-bottom: solid black 1px;
            font-family: 'Courier New';
            font-size: 10.5pt;
            color: black;
        }

    </style>
    <pre class="app">
                                      Державне агентство України
                                      з питань науки, інновацій
                                      та інформації
                                      Україна, 01601, Київ,
                                      бульвар Тараса Шевченка, 16


<strong>                            ЗАЯВА
        про реєстрацію електронного інформаційного ресурсу
</strong>
<br><br>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 49px; ">Заявник</td><td style="width: 100%; white-space: nowrap;"><input type="text" title="Повне найменування юридичної особи власника електронного інформаційного ресурсу згідно зі статутом" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_name1" name="data[provider_name1]" class="app_input" style="width: 491px;"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" name="data[provider_name2]" title="Повне найменування юридичної особи власника електронного інформаційного ресурсу згідно зі статутом" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2" style="text-align:center;">(повне найменування юридичної особи власника електронного</td></tr><tr><td colspan="2" style="text-align:center;"> інформаційного ресурсу згідно зі статутом)</td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 60px;">Керівник</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_manager" name="data[provider_manager]" title="Посада, П.І.Б. керівника юридичної особи" class="app_input" style="width: 480px;" maxlength="53"></td></tr><tr><td colspan="2" style="text-align:center">(посада, П.І.Б. керівника юридичної особи)</td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 240px;">Місцезнаходження юридичної особи *</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_address" name="data[provider_address]" title="Юридична адреса" class="app_input" style="width: 300px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 165px;">Адреса для листування *</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_post_address" name="data[provider_post_address]" title="Адреса для листування" class="app_input" style="width: 375px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 75px;">Телефон **</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_phone" name="data[provider_phone]" title="Телефон. Крім цифр також допускаються символи + - ( ) , ;" class="app_input" style="width: 110px;" maxlength="53"></td><td style="width: 50px;">Факс **</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_fax" name="data[provider_fax]" title="Факс (крім цифр також допускаються символи: + - ( ) , ; )" class="app_input" style="width: 110px;" maxlength="53"></td><td style="width: 45px;">E-mail</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="email" name="data[email]" title="Адреса електронної пошты (англійські літери і знак @)" class="app_input" style="width: 148px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 255px; ">Ідентифікаційний код юридичної особи</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_identification" name="data[provider_identification]" title="Ідентифікаційний код юридичної особи (тільки цифри)" class="app_input" style="width: 285px;" maxlength="53"></td></tr></tbody></table>

Банківські реквізити:

<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 230px;">Поточний рахунок N</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_account" name="data[provider_account]" title="Поточний рахунок (тільки цифри)" class="app_input" style="width: 410px;;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 10px;">У</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_bank" name="data[provider_bank]" title="Найменування кредитної установи" class="app_input" style="width: 530px;" maxlength="53"></td></tr><tr><td colspan="2" style="text-align:center;">(найменування кредитної установи)</td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 25px;">МФО</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="provider_mfo" name="data[provider_mfo]" title="МФО (тільки цифри)" class="app_input" style="width: 515px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 370px;">Прошу зареєструвати електронний інформаційний ресурс</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="name1" name="data[name1]" title="Найменування електронного інформаційного ресурсу" class="app_input" style="width: 170px;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="name2" name="data[name2]" title="Найменування електронного інформаційного ресурсу" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="name3" name="data[name3]" title="Найменування електронного інформаційного ресурсу" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2" style="text-align:center">(найменування електронного інформаційного ресурсу)</td></tr></tbody></table>

Дані про електронний інформаційний ресурс:

<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 70px;">Розробник</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="requisitesdeveloper1" name="data[requisitesdeveloper1]" title="Основні реквізити розробника: найменування, адреса, телефон" class="app_input" style="width: 470px;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="requisitesdeveloper2" name="data[requisitesdeveloper2]" title="Основні реквізити розробника: найменування, адреса, телефон" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2" style="text-align:center;">(основні реквізити розробника: найменування, адреса, телефон) </td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 30px;">Тема</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="topic" name="data[topic]" title="Тема ресурсу" class="app_input" style="width: 510px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 90px;">Доменне ім'я</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="domainname" name="data[domainname]" title="Доменне ім'я ресурсу (англійські літери, а також символы . : //)" class="app_input" style="width: 450px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 95px;">Умови доступу</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="accessconditions" name="data[accessconditions]" title="Умови доступу" class="app_input" style="width: 445px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 60px;">Анотація</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="description1" name="data[description1]" title="Анотація" class="app_input" style="width: 480px;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="description2" name="data[description2]" title="Анотація" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="description3" name="data[description3]" title="Анотація" class="app_input" style="width: 100%;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="description4" name="data[description4]" title="Анотація" class="app_input" style="width: 100%;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 125px;">Ключові слова ***</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="keywords1" name="data[keywords1]" title="Ключові слова і фрази. Для відокремлення ключових слів і фраз використовується кома" class="app_input" style="width: 415px;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="keywords2" name="data[keywords2]" title="Ключові слова і фрази. Для відокремлення ключових слів і фраз використовується кома" class="app_input" style="width: 100%;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 30px;">Мова</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="language" name="data[language]" title="Мова" class="app_input" style="width: 510px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 40px;">Формат</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="format" name="data[format]" title="Формат" class="app_input" style="width: 500px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 195px;">Обсяг інформації (у байтах)</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="sizebytes" name="data[sizebytes]" title="Обсяг інформації у байтах (тільки цифри)" class="app_input" style="width: 345px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 40px;">Джерело</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="source" name="data[source]" title="Джерело" class="app_input" style="width: 490px;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 235px; white-space: nowrap;">Дата надання доступу користувачам
                        <input id="resource_publisheddate_target" name="data[resource_publisheddate]" type="hidden" value="1512683522">
                        <input id="resource_publisheddate_helper" type="hidden" value="1512683522000">
                        <input id="resource_publisheddate_picker" readonly="readonly" class="app_input hasDatepicker" value="07.12.2017" type="text">
                        <script type="text/javascript">
                        $(function() {
                            $( "#resource_publisheddate_picker" ).datepicker({
                                altField: "#resource_publisheddate_helper",
                                altFormat: "@",
                                dateFormat: "dd.mm.yy",
                                onSelect: function(dateText, inst) { $('#resource_publisheddate_target').val(parseInt($('#resource_publisheddate_helper').val())/1000); },
                                monthNames: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
                                monthNamesShort: ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
                                nextText: 'Наст.',
                                prevText: 'Попер.',
                                dayNames: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'],
                                dayNamesMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                dayNamesShort:  ['Нед', 'Пон', 'Вів', 'Сер', 'Чет', 'П\'ят', 'Суб'],
                                firstDay: 1
                            });
                        });
                        </script>
     </td><td> </td>  </tr></tbody></table>
Інші відомості  про  електронний  інформаційний  ресурс   для
внесення   до   Національного  реєстру  електронних  інформаційних
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 235px; white-space: nowrap;">ресурсів за пропозицією власника</td><td><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="other1" name="data[other1]" title="Інші відомості  про  електронний  інформаційний  ресурс" class="app_input" style="width: 305px;" maxlength="53"></td></tr><tr><td colspan="2"><input type="text" onkeyup="return is_max(this);" onfocus="return hint(this)" onblur="return nohint(this)" id="other1" name="data[other2]" title="Інші відомості  про  електронний  інформаційний  ресурс" class="app_input" style="width: 100%;" maxlength="53"></td></tr></tbody></table>
<table style="width: 100%; border-collapse: collapse;"><tbody><tr><td style="width: 1px; white-space: nowrap;">
                        <input id="fromdate_target" name="data[fromdate]" type="hidden" value="1512683522">
                        <input id="fromdate_helper" type="hidden" value="1512683522000">
                        <input id="fromdate_picker" readonly="readonly" class="app_input hasDatepicker" value="07.12.2017" type="text">
                        <script type="text/javascript">
                        $(function() {
                            $( "#fromdate_picker" ).datepicker({
                                altField: "#fromdate_helper",
                                altFormat: "@",
                                dateFormat: "dd.mm.yy",
                                onSelect: function(dateText, inst) { $('#fromdate_target').val(parseInt($('#fromdate_helper').val())/1000); },
                                monthNames: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
                                monthNamesShort: ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
                                nextText: 'Наст.',
                                prevText: 'Попер.',
                                dayNames: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'],
                                dayNamesMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                                dayNamesShort:  ['Нед', 'Пон', 'Вів', 'Сер', 'Чет', 'П\'ят', 'Суб'],
                                firstDay: 1
                            });
                        });
                        </script>
     </td><td>              </td><td>_______________________________</td></tr><tr><td><em>(дата заповнення заяви)</em></td><td>             </td><td><em>      (підпис заявника)
          М.П.</em></td></tr></tbody></table>

<p id="prov_manager" name="provider_manager" style="width: 100%;text-align: right "></p>

*** Ключові слова відокремлюються один від одного за допомогою коми.
    Допускається, також, введення ключових фраз, що складаються з двох
    і більше слів.

    </pre>
    <script type="text/javascript">
        $(function() {
            var publisheddate_picker = {
                dateFormat: "dd.mm.yy",
                onSelect: function(dateText, inst) {  },
                monthNames: ['Січень', 'Лютий', 'Березень', 'Квітень', 'Травень', 'Червень', 'Липень', 'Серпень', 'Вересень', 'Жовтень', 'Листопад', 'Грудень'],
                monthNamesShort: ['Січ', 'Лют', 'Бер', 'Кві', 'Тра', 'Чер', 'Лип', 'Сер', 'Вер', 'Жов', 'Лис', 'Гру'],
                nextText: 'Наст.',
                prevText: 'Попер.',
                dayNames: ['Неділя', 'Понеділок', 'Вівторок', 'Середа', 'Четвер', 'П\'ятниця', 'Субота'],
                dayNamesMin: ['Нд', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
                dayNamesShort:  ['Нед', 'Пон', 'Вів', 'Сер', 'Чет', 'П\'ят', 'Суб'],
                firstDay: 1
            };
            $( "#publisheddate" ).datepicker(publisheddate_picker);
        });
        function is_max(o) {
            var cl = $(o).val().length;
            var ml = $(o).attr('maxlength');
            if(cl >= ml)$(o).next().focus();
            return true;
        }
    </script>
    <center><input class="button lng" action="/" method="POST" value="Заяву підтверджую" type="submit"></center>

</form>
<?php include ('layouts/footer.php'); ?>
