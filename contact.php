<?php include("doctype.php"); ?>
<?php include ('layouts/header.php'); ?>

<div class="header_bg"><!-- start header -->
	<div class="container-fluid">
        <div style="background-color: #F0F7E8" class="header row">
		<nav class="navbar" role="navigation">
		  <div class="container-fluid">
		    <!-- Brand and toggle get grouped for better mobile display -->
		    <div class="navbar-header">
		      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
		        <span class="sr-only">Переключити навігацію</span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		        <span class="icon-bar"></span>
		      </button>
		      <a class="navbar-brand" href="index.php"> </a>
		    </div>
		    <!-- Collect the nav links, forms, and other content for toggling -->
		    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
		      <ul class="menu nav navbar-nav ">
                  <li><a href="index.php"><?php echo $row['title_button']; ?></a></li>
                  <li><a href="feature.php"><?php echo  $row['news_button'] ?> </a></li>
                  <li><a href="blog.php"><?php echo  $row['ir_button'] ?></a></li>
                  <li><a href="about.php"><?php echo  $row['about_button'] ?></a></li>
                  <li class="active"><a href="contact.php"><?php echo  $row['contact_button'] ?></a></li>
		      </ul>
                <form class="navbar-form navbar-right" role="search">
                    <div class="form-group my_search">
                        <input type="text" class="form-control" placeholder="Пошук"><button type="submit" class="btn btn-default"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                </form>
		    </div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		</div>
	</div>
</div>
<div class="main"><!-- start main -->
	<div class="container">
		<div class="row contact"><!-- start contact -->
			<div class="col-md-4">
				<div class="contact_info">
					<h2>Знайдіть нас тут:</h2>
					<div class="map">
						<iframe width="100%" height="175" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14373.803377271503!2d30.48007284035279!3d50.44196753634954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe27d1c9fccd6d92b!2z0JzQuNC90LjRgdGC0LXRgNGB0YLQstC-INCe0LHQvtGA0L7QvdGLINCj0LrRgNCw0LjQvdGL!5e0!3m2!1sru!2sua!4v1507033659765" width="600" height="450" frameborder="0" style="border:0"></iframe><br><small><a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d14373.803377271503!2d30.48007284035279!3d50.44196753634954!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xe27d1c9fccd6d92b!2z0JzQuNC90LjRgdGC0LXRgNGB0YLQstC-INCe0LHQvtGA0L7QvdGLINCj0LrRgNCw0LjQvdGL!5e0!3m2!1sru!2sua!4v1507033659765" width="600" height="450" frameborder="0" style="border:0">Переглянути більшу карту</a></small>
					</div>
				</div>
				<div class="company_ad">
					<h2>ми знаходимось за адресою :</h2>
					<address>
						<p>пр-кт Повітрофлотський 6,</p>
						<p>Київ, 03168</p>
						<p>Телефон: 044 454 4404</p>
						<p>Тел.факс: (044) 226-20-15</p>
						<p>Email: <a href="mailto:info@mycompany.com">zapytmou@gmail.com</a></p>
						<p>Посилання на: <a href="https://ru-ru.facebook.com/theministryofdefence.ua/">Facebook</a>, <a href="https://twitter.com/defenceu">Twitter</a></p>
					</address>
				</div>
			</div>
			<div class="col-md-8">
				<div class="contact-form">
					<h2>Зв'яжіться з нами</h2>
					<form method="post" action="contact.php" >
						<div>
							<span>ім'я</span>
							<span><input type="username" class="form-control" name='userName' id="userName"></span>
						</div>
						<div>
							<span>e-mail</span>
							<span><input type="email" class="form-control" name='inputEmail' id="inputEmail"></span>
						</div>
						<div>
							<span>моб. телефон</span>
							<span><input type="number" class="form-control" name='number' id="number"></span>
						</div>
						<div>
							<span>Що ви хочете запитати?</span>
							<span><textarea name="userMsg"> </textarea></span>
						</div>
						<div>
							<span><input type="submit" value="Відправити"></span>
						</div>
					</form>
				</div>
			</div>
			<div class="clearfix"></div>
		</div> <!-- end contact -->
	</div>
    <?php
    if ($_POST) {
    $connect = connectPDO();
    $statement = $connect->prepare('INSERT INTO contact(id_contact,name_c,email_c,tel_c,question) VALUES (:id_contact,:name_c,:email_c,:tel_c,:question)');
    $statement ->bindParam(':id_contact', $id_contact);
    $statement ->bindParam(':name_c', $name_c);
    $statement ->bindParam(':email_c', $email_c);
    $statement ->bindParam(':tel_c', $tel_c);
    $statement ->bindParam(':question', $question);
    $id_contact = 0;
    $name_c = $_POST['userName'];;
    $email_c = $_POST['inputEmail'];
    $tel_c = $_POST['number'];
    $question = $_POST['userMsg'];
    $statement->execute();
    }
?>
<?php include ("layouts/footer.php");?>