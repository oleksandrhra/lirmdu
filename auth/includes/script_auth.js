function surnameValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("surname");
    if (!reg.test(val.value) && (val.value.length > 2)) {
        val.className = "input";
    } else {
        val.className = "error-input";
    }
};

function nameValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("name");
    if (!reg.test(val.value) && (val.value.length > 1)) {
        val.className = "input";
    } else {
        val.className = "error-input";
    }
};

function emailValid() {
    var reg=/[0-9a-z_]+@[0-9a-z_]+\.[a-z]{2,5}/i;
    var regRu = /[0-9a-z_]+@[0-9a-z_]+\.[ru]/i;
    var val = document.getElementById("email");
    if (reg.test(val.value) ) {
        val.className = "input";
    } else {
        val.className = "error-input";
    }

    if (regRu.test(val.value)) {
        val.className = "error-input";
    }
};

function telValid() {
    var reg=/(?:\w)(?:(?:(?:(?:\+?3)?8\W{0,5})?0\W{0,5})?[34569]\s?\d[^\w,;(\+]{0,5})?\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d\W{0,5}\d(?!(\W?\d))/;
    var regNaN = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("tel");
    if ( (reg.test(val.value)) && (val.value.length == 13)) {
        val.className = "input";
    } else {
        val.className = "error-input";
    }
};

function capchaValid() {
    var reg = new RegExp("^.*[^A-zА-яЁё].*$");
    var val = document.getElementById("capcha");
    if (reg.test(val.value)) {
        val.className = "input";
    } else {
        val.className = "error-input";
    }
};

