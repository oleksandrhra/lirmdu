<?php 
	ob_start();
	session_start();
	require "includes/connection.php";
    include("includes/header.php");
	$user = R::findOne("user","login = ?", array($_SESSION["username"]));
	if (!$user) {
		header("Location: index.php");
	}
	$data = $_POST;
	if (isset($data["change"])) {
		$errors = array();
		if ($data["surname"] == "") {
			$errors[] = "Введіть прізвище";
		}
		if ($data["name"] == "") {
			$errors[] = "Введіть імя";
		}
		if ($data["tel"] == "") {
			$errors[] = "Введіть телефон";
		}
		if (strlen($data["tel"]) != 10) {
			$errors[] = "Введіть номер телефону згідно зразку";
		}
		if ($data["password"] != $data["password_2"]) {
			$errors[] = "Нові паролі не співпадають";
		}
		if ($data["capcha"] == "") {
			$errors[] = "Введіть капчу";
		}
		if (md5($data["capcha"]) != $_SESSION['randomnr2']) {
			$errors[] = "Введіть капчу повторно";
		}
		if ($data["password_old"] == "") {
			$errors[] = "Для зміни данних введіть старий пароль";
		}
		if ((hash("sha256", $data["password_old"]) == $user->password) && empty($errors) && (md5($data["capcha"]) == $_SESSION['randomnr2'])) {
			$user->password = hash("sha256", $data["password"]);
			$user->name = $data["name"];
			$user->surname = $data["surname"];
			$user->tel = $data["tel"];
			R::store($user);
			echo "<div style = \"color: green\">Дані змінено успішно</div>";
			unset($data);
		}
		else{
			$errors[] = "Старий пароль введено не вірно";
		}
			

		if (!empty($errors)) {
			echo "<div class=\"error\">" . "ПОВІДОМЛЕННЯ: ". array_shift($errors) . "</div>";
		}
	}
?>
    <div class="container mregister">
    <div id="login">

<h1>Особистий кабінет</h1>
<form name="registerform" action="change.php" method="POST">
	<p>
		<label for="surname">Прізвище<br>
		<input class="input" type="text" name="surname" id="surname" class="input" size="32" value="<?php echo $user->surname;?>"></label>
	</p>
	<p>
		<label for="name">Імя<br>
		<input class="input" type="text" name="name" id="name" size="32" value="<?php echo $user->name;?>">
		</label>
	</p>
	
	<p>
		<label for="email">Email<br>
		<input class="input" type="email" name="email" id="email" value="<?php echo $user->email; ?>" size="32" disabled></label>
	</p>
	<p>
		<label for="tel">Номер телефону<br>
		<input class="input" type="text" name="tel" id="tel" value="<?php echo $user->tel; ?>" size="32"></label>
	</p>
	
	<p>
		<label for="password">Новий пароль<br>
		<input class="input" type="password" name="password" id="password" value="" size="32"></label>
	</p>	
	<p>
		<label for="password_2">Введіть новий пароль повторно<br>
		<input class="input" type="password" name="password_2" id="password_2" value="" size="32"></label>
	</p>
	<p>
		<label for="password_old">Введіть старий пароль<br>
		<input class="input" type="password" name="password_old" id="password_old" value="" size="32"></label>
	</p>
	<p>
		<img src = "captcha.php" alt = "Каптча"><br>
		<label for="capcha">Введіть текст із зображенння</label>
  		<input class="input" type = "text" name = "capcha" value = "" id = "capcha" size = "10">
	</p>
	<p>
        <a href="index.php">На головну</a><input class="button" type="submit" name="change" value="Змінити">
    </p>

</form>
    </div>
    </div>
<?php 
	ob_flush();
?>