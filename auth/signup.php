<?php 
	session_start();
	ob_start();
    date_default_timezone_set('Europe/Kiev');
	require "includes/connection.php";
	include("send.php");
	include("includes/header.php");
	$data = $_POST;
	if (isset($data["signup"])) {
		$errors = array();
		if ($data["surname"] == "") {
			$errors[] = "Введіть прізвище";
		}
		if ($data["name"] == "") {
			$errors[] = "Введіть імя";
		}
		if ($data["email"] == "") {
			$errors[] = "Введіть email";
		}
		if ($data["tel"] == "") {
			$errors[] = "Введіть телефон";
		}
		if (strlen($data["tel"]) != 13) {
			$errors[] = "Введіть номер телефону згідно зразку";
		}
		if ($data["username"] == "") {
			$errors[] = "Введіть логін";
		}
		if (R::count("user", "login = ?", array($data["username"]))) {
			$errors[] = "Користувач з таким логіном вже існує";
		}
		if ($data["password"] == "") {
			$errors[] = "Введіть пароль";
		}
		if ($data["password"] != $data["password_2"]) {
			$errors[] = "Паролі не співпадають";
		}
		if ($data["capcha"] == "") {
			$errors[] = "Введіть капчу ";
		}
		if (md5($data["capcha"]) != $_SESSION['randomnr2']) {
			$errors[] = "Введіть капчу повторно";
		}
		if (empty($errors)) {
			$user = R::dispense("user");
			$user->login = $data["username"];
			$user->password = hash("sha256", $data["password"]);
			$user->name = $data["name"];
			$user->surname = $data["surname"];
			$user->email = $data["email"];
			$user->tel = $data["tel"];
			$user->reg_time = date(DATE_RSS);
			R::store($user);
			if(Send() == 1) {
				echo "<div class=\"error\">На ваш почтовий адрес відправлено повідомлення с підтвердженям</div>";
			} else{
				echo "<div class=\"error\">Повідомлення на почтовий адрес не відправлено. Сбробуйте пізніше!!!</div>";
			}
			unset($data);
		} else {
            echo "<div class=\"error\">" . "ПОВІДОМЛЕННЯ: ". array_shift($errors) . "</div>";
		}
	}
?>
    <div class="container mregister">
    <div id="login">
	<h1>РЕЄСТРАЦІЯ</h1>
<form name="registerform" action="signup.php" method="post">
	<p>
		<label for="surname">Прізвище<br >
		<input class="input" type="text" name="surname" id="surname" size="32" value="" autocomplete="off" onchange="surnameValid();"></label>
	</p>
	<p>
		<label for="name">Імя<br>
		<input class="input" type="text" name="name" id="name" size="32" value="" autocomplete="off" onchange="nameValid();"></label>
	</p>
	
	<p>
		<label for="email">Email<br>
		<input class="input" type="email" name="email" id="email" value="" size="32" autocomplete="off" onchange="emailValid()"></label>
	</p>
	<p>
		<label for="tel">Номер телефону<br>
		<input class="input" type="text" name="tel" id="tel" value="" size="32" autocomplete="off" onchange="telValid()"></label>
	</p>
	
	<p>
		<label for="username">Логін<br>
		<input class="input" type="text" name="username" id="username" value="" size="20" autocomplete="off"></label>
	</p>
	
	<p>
		<label for="password">Пароль<br>
		<input class="input" type="password" name="password" id="password" value="" size="32" autocomplete="off"></label>
	</p>	
	<p>
		<label for="password_2">Введіть пароль повторно<br />
		<input class="input" type="password" name="password_2" id="password_2" value="" size="32" autocomplete="off"></label>
	</p>
	<p>
		<img src = "captcha.php" alt = "Каптча"><br>
		<label for="capcha">Введіть текст із зображенння</label>
  		<input class="input" type = "text" name = "capcha" value = "" id = "capcha" size = "10" autocomplete="off" onchange="capchaValid()">
	</p>
	<p>
		<input class="button" type="submit" name="signup" value="ЗАРЕГІСТРУВАТИСЯ" onclick="progresBar();">
	</p>
	<p>Ви вже зареєстровані? <br><a href="login.php" >Вхід тут</a>!</p>
    <p style="float: right;"><a href="/index.php">На головну</a></p>
</form>
    </div>
    </div>
<?php 
	ob_flush();
    include("includes/footer.php");
?>