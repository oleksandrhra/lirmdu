<?php
    ob_start();
    session_start();
    require "includes/connection.php";
    include("includes/header.php");
    $data = $_POST;
    if (isset($data["login"])) {
        $errors = array();
        $user = R::findOne("user", "login = ?", array($data["username"]));
        if ($data["username"] == "") {
            $errors[] = "Введіть логін";
        }
        if ($data["password"] == "") {
            $errors[] = "Введіть пароль";
        }
        if (md5($data["capcha"]) != $_SESSION['randomnr2']) {
            $errors[] = "Введіть капчу повторно";
        }
        if ($user){
            if (($user->password == hash("sha256", $data["password"])) && (md5($data["capcha"]) == $_SESSION['randomnr2'])) {
                $_SESSION["username"] = $data["username"];
                $_SESSION["password"] = hash("sha256", $data["password"]);
                $_SESSION["law"] = $user->law;
                header("Location: /index.php");
            }
            else{
                $errors[] = "Пароль введений не вірно";
            }
        }
        else {
            $errors[] = "Користувача з таким логіном не знайдено";
        }
        if (!empty($errors)) {
            echo "<div class=\"error\">" . "ПОВІДОМЛЕННЯ: ". array_shift($errors) . "</div>";
        }
    }
?>
<div class="container mlogin">
<div id="login">
<h1>ВХІД</h1>
<form name="loginform" action="login.php" method="POST">
    <p>
        <label for="username">Логін<br />
        <input class="input" type="text" name="username" id="username" value="" size="20" autocomplete="off"></label>
    </p>
    <p>
        <label for="password">Пароль<br />
        <input class="input" type="password" name="password" id="password" value="" size="20" autocomplete="off"></label>
    </p>
    <p>
        <img src = "captcha.php" alt = "Каптча"><br>
        <label for="capcha">Введіть текст із зображенння</label>
        <input class="input" type = "text" name = "capcha" value = "" id = "capcha" size = "10" autocomplete="off">
    </p>
    <p>
        <input class="button" type="submit" name="login" value="Вхід" />
    </p>
    <p>Ще немає облікового запису? <a href="signup.php" >Зареєструйтесь тут</a>!</p>
    </form>
</div>
</div>
<?php
    include 'includes/footer.php';
    ob_flush();
?>